import { ButtonHTMLAttributes } from 'react'

export default function Button({
  className,
  children,
  ...attrs
}: ButtonHTMLAttributes<HTMLButtonElement>) {
  return (
    <button
      className='text-sm font-semibold bg-white border text-gray-900 py-3 px-4 rounded-lg hover:bg-gray-300 hover:text-gray-700 focus:outline-none focus-visible:ring-2 focus-visible:ring-white focus-visible:ring-offset-2 focus-visible:ring-offset-gray-900'
      type="submit"
      {...attrs}
    >
      {children}
    </button>
  )
}
