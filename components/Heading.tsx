import Link from 'next/link'

interface Props {
  returnLink?: string
  heading: string
}

export default function Heading({ returnLink, heading }: Props) {
  return (
    <>
      {returnLink && (
        <Link href={returnLink}>
          <a className='underline text-gray-600'>Back</a>
        </Link>
      )}
      <h1 className='text-4xl uppercase text-gray-900 font-sans font-black tracking-wide text-center'>
        {heading}
      </h1>

      <hr className='border-2 border-gray-700 mt-4' />
    </>
  )
}
