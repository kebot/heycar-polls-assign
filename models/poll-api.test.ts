// here uses real api instead of mocked one in real world application
import { getQuestions, vote, createPoll, getQuestionId, getChoiseId } from './poll-api'
import fetch from 'isomorphic-unfetch'

test('Can vote on first poll', async () => {
  window.fetch = fetch

  const questions = await getQuestions()

  expect(questions.length).toBeTruthy()

  const question = questions[0]
  const firstChoice = question.choices[0]

  const voteResult = await vote(getQuestionId(question), getChoiseId(firstChoice))

  expect(voteResult.url).toBe(firstChoice.url)
})

test('Can create poll', async () => {
  window.fetch = fetch

  const newPoll = await createPoll({
    question: 'Favorite programming langauge?',
    choices: ['Swift', 'Python', 'Objective-C', 'Ruby']
  })

  expect(newPoll.url).toBeTruthy()
})
