import create from 'zustand'
import { QuestionID, ChoiseID, Question, getQuestions, vote, getQuestionId, getPoll, QuestionPost, createPoll, getChoiseId  } from './poll-api'
import produce from 'immer'

export { getChoiseId, getQuestionId } from './poll-api'
export type { Question, Choice, QuestionPost } from './poll-api'

interface PollAppModel {
  byId: Record<QuestionID, Question>
  list: QuestionID[]
  myVote: Record<QuestionID, ChoiseID>

  // actions
  fetchAllPolls: () => Promise<void>
  fetchSinglePoll: (questionId: QuestionID) => Promise<void>
  vote: (questionId: QuestionID, choiceId: ChoiseID) => Promise<void>
  createPoll: (body: QuestionPost) => Promise<Question>
}

export default create<PollAppModel>((set, get) => ({
  byId: {},
  myVote: {},
  list: [],

  fetchAllPolls: async () => {
    if (get().list.length > 0) {
      return 
    }

    const questions = await getQuestions(10)

    const byId: Record<QuestionID, Question> = {}
    const list = []

    for (let question of questions) {
      const qid = getQuestionId(question)
      byId[qid] = question
      list.push(qid)
    }

    set({ byId: byId, list: list })
  },

  fetchSinglePoll: async (qid: QuestionID) => {
    const poll = await getPoll(qid)

    set(produce((state: PollAppModel) => {
      state.byId[getQuestionId(poll)] = poll
    }))
  },

  vote: async (questionId: QuestionID, choiceId: ChoiseID) => {
    const myChoice = await vote(questionId, choiceId)

    set(produce((state: PollAppModel) => {
      state.myVote[questionId] = choiceId
      state.byId[questionId].choices.forEach((choice) => {
        if (getChoiseId(choice) === choiceId) {
          choice.votes += 1
        }
      })
    }))
  },

  createPoll: async (body: QuestionPost) => {
    const poll = await createPoll(body)

    set(produce((state: PollAppModel) => {
      state.byId[getQuestionId(poll)] = poll
      state.list.push(getQuestionId(poll))
    }))

    return poll
  }
}))
