import { renderHook, act } from '@testing-library/react-hooks'
import usePollStore from './poll-store'
import fetch from 'isomorphic-unfetch'

describe('poll store', () => {
  beforeAll(() => {
    // in real case, use fetchMock instead
    window.fetch = fetch
  })

  it('should able to list all polls', async () => {
    const { result } = renderHook(() => usePollStore(state => state))

    await act(async () => {
      await result.current.fetchAllPolls()
    })

    expect(result.current.list.length).toBeGreaterThan(0)
  })

  it('should able to add polls', async () => {
    const { result } = renderHook(() => usePollStore(state => state))

    await act(async () => {
      await result.current.createPoll({ question: 'What is fav food?', choices: ['fish', 'meat', 'wood'] })
    })

    expect(result.current.list.length).toBeGreaterThan(0)
  })
})