import * as yup from 'yup'
const API_ROOT = 'https://private-ee75d-pollsapi.apiary-mock.com'

export type ChoiseID = string
export type QuestionID = string

export interface Choice {
  choice: string
  votes: number
  url: string
}

export interface Question {
  question: string
  choices: Choice[]
  published_at: string
  url: string
}

export interface QuestionPost {
  question: string,
  choices: string[]
}

export let QuestionPostSchema = yup.object().shape({
  question: yup.string()
    .min(3, 'question should have at least 3 letters')
    .defined(),
  choices: yup.array().of(yup.string().required()).min(2, 'Poll must have at least 2 choices')
})

export function getChoiseId(choise: Choice): ChoiseID {
  return choise.url.split('/').slice(-1).pop()
}

export function getQuestionId(question: Question): QuestionID {
  return question.url.split('/').slice(-1).pop()
}

export async function getPoll(questionId: QuestionID): Promise<Question> {
  const poll = await (await fetch(`${API_ROOT}/questions/${questionId}`)).json()

  // this is the problem of example data for POLL1
  if (questionId === '1' && poll.choices[0]['colour']) {
    poll.choices[0]['choice'] = 'Swift'
    delete poll.choices[0]['colour']
  }

  return poll
}

export async function vote (questionId: QuestionID, choiceId: ChoiseID): Promise<Choice> {
  return await (await fetch(`${API_ROOT}/questions/${questionId}/choices/${choiceId}`, {
    method: 'POST'
  })).json()
}

export async function getQuestions (page: number = 1): Promise<Question[]> {
  return await (await fetch(`${API_ROOT}/questions?${page}`)).json()
}

export async function createPoll (body: QuestionPost): Promise<Question> {
  const poll: Question = await (await fetch(`${API_ROOT}/questions`, {
    method: 'POST',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(body)
  })).json()

  poll.url = '/questions/' + Math.round(Math.random() * 100)
  poll.question = body.question
  poll.choices = body.choices.map((text, i) => {
    return {
      choice: text,
      votes: 0,
      url: `${poll.url}/choices/${i}`
    }
  })

  return poll

}