import { useEffect, useState } from 'react'
import Head from 'next/head'
import usePollStore, { Question, Choice, getQuestionId, getChoiseId } from '../../models/poll-store'
import { useRouter } from 'next/router'
import Button from '../../components/Button'
import Heading from '../../components/Heading'

const Home = () => {
  const store = usePollStore()

  const router = useRouter()
  const { id } = router.query

  useEffect(() => {
    store.fetchSinglePoll(id as string)
  }, [id])

  const poll: Question | undefined = store.byId[id as string]
  const totalVotes = poll ? poll.choices.reduce((sum, choice) => sum + choice.votes, 0) : 1

  const [selected, setSelected] = useState<Choice | undefined>(null)

  const myVote = store.myVote[id as string]

  return (
    <div className='container mx-auto px-4 py-4'>
      <Head>
        <title>{poll ? poll.question : 'Loading'}</title>
        <link rel='icon' href='/favicon.ico' />
      </Head>

      {!poll && <div>Loading...</div>}

      {poll && (
        <main>
          <Heading returnLink='/' heading={poll.question} />

          <div>
            {poll.choices.map((choise) => {
              return (
                <div
                  key={choise.url}
                  className={`flex justify-evenly border rounded my-10 px-10 py-2 sm:mx-20 cursor-pointer ${
                    selected?.url === choise.url ? 'bg-gray-100' : 'bg-white'
                  } ${myVote === getChoiseId(choise) ? 'bg-blue-100' : 'bg-white'}`}
                  onClick={() => !myVote && setSelected(choise)}
                >
                  <div className='w-full'>{choise.choice}</div>
                  <div className='w-full text-blue'>{choise.votes} votes</div>
                  <div className='w-full'>
                    {totalVotes === 0 ? '-' : Math.round((choise.votes / totalVotes) * 100) + '%'}
                  </div>
                </div>
              )
            })}
          </div>

          {selected && !myVote && (
            <div className='text-right'>
              <Button onClick={() => store.vote(getQuestionId(poll), getChoiseId(selected))}>
                Save Vote
              </Button>
            </div>
          )}
          {process.env.NODE_ENV === 'development' && <pre>DEBUG: {JSON.stringify(store, null, 2)}</pre>}
        </main>
      )}
    </div>
  )
}

export default Home
