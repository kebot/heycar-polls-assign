import Head from 'next/head'
import usePollStore, { Question, QuestionPost } from '../../models/poll-store'
import { useRouter } from 'next/router'
import { useField, Formik, Form, FastField, FieldArray, ErrorMessage } from 'formik'
import { QuestionPostSchema } from '../../models/poll-api'
import Button from '../../components/Button'
import Heading from '../../components/Heading'

const Input: React.FC<{ label: string; name: string } & any> = ({ label, name, ...props }) => {
  return (
    <div className='w-full'>
      {label && (
        <label className='block text-gray-700 text-sm font-bold mb-2' htmlFor={name}>
          {label}
        </label>
      )}
      <FastField
        className='shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline'
        name={name}
        {...props}
      />
    </div>
  )
}

const InputArray: React.FC<{ name: string }> = ({ name }) => {
  const [field] = useField(name)

  return (
    <FieldArray
      name={name}
      render={(arrayHelpers) => {
        return (
          <div>
            {(field.value || []).map((field: string, index: number) => {
              return (
                <div key={index} className='flex justify-between items-center mb-4'>
                  <Input type='text' name={`${name}.${index}`} />
                  <button
                    type='button'
                    onClick={() => {
                      arrayHelpers.remove(index)
                    }}
                    className='w-8 h-8 border rounded-full ml-4 hover:bg-blue-50'
                  >
                    x
                  </button>
                </div>
              )
            })}

            <Button onClick={() => arrayHelpers.push('')}>+ (add a answer)</Button>
          </div>
        )
      }}
    />
  )
}

const New = () => {
  const store = usePollStore()

  const router = useRouter()

  const onSubmit = async (data) => {
    const { url } = await store.createPoll(data)
    router.push(url)
  }

  return (
    <div className='container mx-auto px-4 py-4'>
      <Head>
        <title>Created a new Form</title>
        <link rel='icon' href='/favicon.ico' />
      </Head>

      <main>
        <Heading returnLink='/' heading='Create new poll' />

        <Formik
          initialValues={{ question: '', choices: [] } as QuestionPost}
          onSubmit={onSubmit}
          validationSchema={QuestionPostSchema}
        >
          <Form>
            <Input label='Question' placeholder='Question...' name='question' />
            <div className='text-red-400'>
              <ErrorMessage name='question' />
            </div>

            <div className='mt-4 mb-4'>
              <h3>Choices:</h3>
              <div className='text-red-400'>
                <ErrorMessage name='choices' />
              </div>
              <InputArray name='choices' />
            </div>

            <Button
              className='cursor-pointer text-sm font-semibold bg-white border text-gray-900 py-3 px-4 rounded-lg hover:bg-gray-300 hover:text-gray-700 focus:outline-none focus-visible:ring-2 focus-visible:ring-white focus-visible:ring-offset-2 focus-visible:ring-offset-gray-900'
              type='submit'
            >
              Create Poll
            </Button>
          </Form>
        </Formik>
      </main>
    </div>
  )
}

export default New
