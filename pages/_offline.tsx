export default () => {
  return <div>You are offline! Try again when you are online.</div>
}