import { useEffect } from 'react'
import Head from 'next/head'
import usePollStore, { Question } from '../models/poll-store'
import Link from 'next/link'
import Heading from '../components/Heading'

const QuestionItem: React.FC<{ question: Question }> = ({ question }) => {
  return (
    <Link href={question.url}>
      <a className='border rounded-xl p-4 hover:bg-gray-50 hover:cursor-pointer hover:shadow'>
        <h3 className='text-xl font-serif'> {question.question} </h3>
        <p className='text-sm'>{new Date(question.published_at).toUTCString()}</p>
        <div className='text-sm'>{question.choices.length} Choices</div>
      </a>
    </Link>
  )
}

const Home = () => {
  const store = usePollStore()

  useEffect(() => {
    store.fetchAllPolls()
  }, [])

  return (
    <div className='container mx-auto px-4 py-4'>
      <Head>
        <title>Questions</title>
        <link rel='icon' href='/favicon.ico' />
      </Head>

      <main>
        <Heading heading='Questions'></Heading>

        <div className='grid sm:grid-cols-2 lg:grid-cols-3 py-9 gap-4'>
          {store.list.length === 0 && <div>Loading...</div>}

          {store.list.map((qid) => {
            const question = store.byId[qid]
            if (!question) {
              return null
            }

            return <QuestionItem key={qid} question={question} />
          })}

          <Link href='/questions/new'>
            <a className='text-8xl border rounded-xl p-4 hover:bg-gray-50 cursor-pointer hover:shadow flex items-center justify-center'>
              +
            </a>
          </Link>
        </div>

        {process.env.NODE_ENV === 'development' && <pre>{JSON.stringify(store, null, 2)}</pre>}
      </main>
    </div>
  )
}

export default Home
