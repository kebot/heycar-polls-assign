# Introduction

Features:

* List of questions page ( http://pasteboard.co/8AReDrPvo.jpg )
* Question detail page ( http://pasteboard.co/8ARC7212L.jpg )
* Create a new question page.
* Handle offline mode (your application still need to return something when offline).
  * it has basic offline support with next-pwa, it's not perfect but works.

Requirements:

* You need to use a state management library.
  * `zustand` hook support is nice
* You need to use any CSS-in-js library you like but please avoid UI library like bootstrap.
  * NextJS has `StyledJSX` natively supported, but I'm using `TailwindCSS` here.
* Your application needs to be responsive and developed in a mobile-first approach.
  * It's not perfect, but has some responsive inside.
* Your application needs to be tested on a decent coverage level using a testing library of your choice.
  * Jest + testing library, I mainly testing models and hooks instead of testing the whole UI, I feel it's a better way to test application.

* the application should be deployed.
  * it's deployed in Now: https://poll-assign.vercel.app/


# How to start/test

View it without start locally: https://poll-assign.vercel.app/

## Run dev server

```bash
npm dev
```

Or

```bash
npm dev
```

## Run Jest Tests

```bash
npm test
```
